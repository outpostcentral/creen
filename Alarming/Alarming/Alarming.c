#include <stdio.h>
#include "file.h"

int main(int argc, const char* argv[])
{
	long size;
	const char * path;
	if(argc != 2)
	{
		printf("Usage: Alarming <inputfile>\r\n");
		return 1;
	}
	path = argv[1];
	size = file_size(path);
	if(size == -1)
	{
		printf("Unknown file: %s", path);
		return 1;
	}

	// Entry point...start coding from here.
	 
    return 0;
}

