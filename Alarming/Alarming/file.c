#include "file.h"
#include <stdio.h>

char* file_read_all_text(const char* path, char* buffer, const int max_buffer_size)
{
	FILE * fp = fopen(path, "r");
	buffer[fread(buffer, sizeof(char), max_buffer_size-1, fp)] = 0;
	fclose(fp);
	return buffer;
}

long file_size(const char* path)
{
	FILE* fp;
	long size;
	fp = fopen(path, "r");
	if (fp == NULL) return -1;
	fseek(fp, 0L, SEEK_END);
	size = ftell(fp);
	fclose(fp);
	return size;
}
