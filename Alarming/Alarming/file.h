#pragma once

char* file_read_all_text(const char* path, char* buffer, const int max_buffer_size);
long file_size(const char* path);
